package pl.edu.wat.tpid.database

import pl.edu.wat.tpid.model.Certificate
import pl.edu.wat.tpid.model.PersonalTrainer

object Database {

    val getAllTrainers = arrayListOf(
        PersonalTrainer(
            firstName = "Adam",
            lastName = "Abacki",
            phone = "234543234",
            address = "Warszawa, Bialostocka 19",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "Certyfikat Pierwszy",
                    type = "Typ jakis",
                    date = "23.03.2022",
                ),
                Certificate(
                    name = "Certyfikat Drugi",
                    type = "Typ jakis dwa",
                    date = "24.02.2019",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "Bartek",
            lastName = "Babacki",
            phone = "123456789",
            address = "Warszawa, Cybaernetyki 2",
            age = 39,
            certificates = arrayListOf(
                Certificate(
                    name = "Certyfikat Trzeci",
                    type = "Typ jakis trzy",
                    date = "21.04.1998",
                ),
                Certificate(
                    name = "Certyfikat Czwarty",
                    type = "Typ jakis cztery",
                    date = "25.09.2022",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(
                Certificate(
                    name = "name",
                    type = "type",
                    date = "date",
                ),
            ),
        ),
        PersonalTrainer(
            firstName = "firstName",
            lastName = "lastName",
            phone = "phone",
            address = "address",
            age = 24,
            certificates = arrayListOf(),
        ),
    )

}