package pl.edu.wat.tpid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import pl.edu.wat.tpid.R
import pl.edu.wat.tpid.model.PersonalTrainer

class TrainerDetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainer_details)

        val personalTrainer = intent.getParcelableExtra<PersonalTrainer>("DATA")
        val nameSurname = personalTrainer!!.firstName + " " + personalTrainer.lastName

        val nameLastName = findViewById<TextView>(R.id.tv_name_surname)
        val phoneNumber = findViewById<TextView>(R.id.tv_phone)
        val address = findViewById<TextView>(R.id.tv_address)
        val age = findViewById<TextView>(R.id.tv_age)

        nameLastName.text = nameSurname
        phoneNumber.text = personalTrainer.phone
        address.text = personalTrainer.address
        age.text = personalTrainer.age.toString()
        
        val arrayAdapter: ArrayAdapter<*>
        val certificates = (personalTrainer.certificates)


        var mListView = findViewById<ListView>(R.id.lv_certificates)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, certificates)
        mListView.adapter = arrayAdapter





        Toast.makeText(this, personalTrainer.toString(), Toast.LENGTH_LONG).show()
    }
}