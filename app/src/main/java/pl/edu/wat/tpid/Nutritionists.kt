package pl.edu.wat.tpid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.edu.wat.tpid.database.DatabaseNutri
import pl.edu.wat.tpid.view.NutritionistsDetails

class Nutritionists : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nutritionists)

        val nutritionistsList = findViewById<RecyclerView>(R.id.nutritionists_list)

        val nutritionistsAdapter = PersonalTrainerAdapter ()
        nutritionistsAdapter.setOnItemChildClickListener{adapter, view, position ->
            val nutritioniststmp = DatabaseNutri.getAllNutritionists[position]
            val intent = Intent(this, NutritionistsDetails::class.java)
            intent.putExtra("DATA", nutritioniststmp)
            startActivity(intent)
        }

        nutritionistsList.layoutManager = LinearLayoutManager(this)
        nutritionistsList.adapter = nutritionistsAdapter

        nutritionistsAdapter.setNewInstance(DatabaseNutri.getAllNutritionists)
    }
}