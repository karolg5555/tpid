package pl.edu.wat.tpid

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import pl.edu.wat.tpid.view.DietsActivity

class Start : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val trainers = findViewById<Button>(R.id.btn_trainers)
        trainers.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))


            startActivity(intent)
        }
        val nutritionist = findViewById<Button>(R.id.btn_nutrionists)
        nutritionist.setOnClickListener {
            startActivity(Intent(this, Nutritionists::class.java))
        }

    }


}