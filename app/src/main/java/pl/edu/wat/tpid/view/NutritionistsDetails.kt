package pl.edu.wat.tpid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import pl.edu.wat.tpid.R
import pl.edu.wat.tpid.model.PersonalTrainer

class NutritionistsDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nutritionists_details)

        val personalTrainer = intent.getParcelableExtra<PersonalTrainer>("DATA")
        val nameSurname = personalTrainer!!.firstName + " " + personalTrainer.lastName

        val nameLastName = findViewById<TextView>(R.id.tv_name_surnamen)
        val phoneNumber = findViewById<TextView>(R.id.tv_phonen)
        val address = findViewById<TextView>(R.id.tv_addressn)
        val age = findViewById<TextView>(R.id.tv_agen)

        nameLastName.text = nameSurname
        phoneNumber.text = personalTrainer.phone
        address.text = personalTrainer.address
        age.text = personalTrainer.age.toString()

        val arrayAdapter: ArrayAdapter<*>
        val certificates = (personalTrainer.certificates)


        var mListView = findViewById<ListView>(R.id.lv_certificatesn)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, certificates)
        mListView.adapter = arrayAdapter





        Toast.makeText(this, personalTrainer.toString(), Toast.LENGTH_LONG).show()
    }
}